//
//  Emotion.h
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 31.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Emotion : NSObject
@property NSString *name;
@property NSURL *link;
@property NSString *id;
@end
