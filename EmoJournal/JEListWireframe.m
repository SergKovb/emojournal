//
//  EmotionListWireframe.m
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 31.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import "JEListWireframe.h"
#import "MainWireframe.h"
#import "JEListViewController.h"
#import "JEListPresenter.h"

static NSString *ListViewControllerIdentifier = @"JEListViewController";

@interface JEListWireframe ()

@property (nonatomic, strong) JEListViewController *listViewController;

@end



@implementation JEListWireframe

- (void)presentListInterfaceFromWindow:(UIWindow *)window {
    NSLog(@"EmotionListWireframe -> presentListInterfaceFromWindow");
    JEListViewController *listViewController = [self listViewControllerFromStoryboard];
    NSLog(@"listViewController %@",listViewController);
    listViewController.eventHandler = self.listPresenter;
    self.listPresenter.userInterface = listViewController;
    self.listViewController = listViewController;
    
    [self.mainWireframe showRootViewController:listViewController
                                      inWindow:window];
}


- (JEListViewController *)listViewControllerFromStoryboard {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:[NSBundle mainBundle]];
    JEListViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:ListViewControllerIdentifier];
    
    return viewController;
}


@end
