//
//  EmotionListInterator.m
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import "JEListInteractor.h"
#import "AFNetworking.h"
#import "JournalEntry.h"

@interface JEListInteractor ()
@property NSArray *arrayOfJournalEntries;

@end



@implementation JEListInteractor

- (id)init {
    if ((self = [super init])) {
        [self loadEmotionList];
    }
    
    return self;
}


-(void)loadJournalEntriesList {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSURL *URL = [NSURL URLWithString:@"http://emojournal.nickbunich.pro/api/journalEntries"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            _arrayOfJournalEntries = [self arrayOfModelsFromData:responseObject];
            [self sendArrayOfJournalEntries];
        }
    }];
    [dataTask resume];
}

-(void)loadEmotionList {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSURL *URL = [NSURL URLWithString:@"http://emojournal.nickbunich.pro/api/emotions"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSLog(@"emotions %@", jsonDictionary);
        }
    }];
    [dataTask resume];
}

-(NSArray *)arrayOfModelsFromData:(NSData *)data {
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSMutableArray *arrayOfJournalEntries = [NSMutableArray new];
    if (jsonDictionary && jsonDictionary[@"_embedded"] && jsonDictionary[@"_embedded"][@"journalEntries"] ) {
        NSArray *arrayOfEntries = jsonDictionary[@"_embedded"][@"journalEntries"];
        for (NSDictionary *journalEntriesDictionary in arrayOfEntries) {
            [arrayOfJournalEntries addObject:[[JournalEntry alloc]  initWithDictionary:journalEntriesDictionary]];
        }
    }
    return arrayOfJournalEntries;
}


- (void)sendArrayOfJournalEntries {
    [self.output foundJournalEntries:_arrayOfJournalEntries];
}


@end
