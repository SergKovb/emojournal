//
//  EmotionListViewInterface.h
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 31.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JEListViewInterface <NSObject>

- (void)showNoEntriesMessage;
- (void)showEntries:(NSArray *)arrayOfJournalEntries;
- (void)reloadEntries;

@end
