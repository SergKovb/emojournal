//
//  EmotionListViewController.h
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JEListPresenter.h"
#import "JEListViewInterface.h"
#import "JEListModuleInterface.h"

@interface JEListViewController : UITableViewController <JEListViewInterface>
@property (nonatomic, strong) IBOutlet UIView*              noContentView;
@property (nonatomic, strong) JEListPresenter *presenter;
@property (nonatomic, strong) id <JEListModuleInterface>    eventHandler;

@end
