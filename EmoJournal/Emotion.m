//
//  Emotion.m
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 31.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import "Emotion.h"

@implementation Emotion


-(instancetype)initWithDictionary:(NSDictionary *)jsonDictionary {
    self = [super init];
    if (self && [self validateEmotion:jsonDictionary]) {
        _id = jsonDictionary[@"id"];
        _name = jsonDictionary[@"name"];
        _link = [NSURL URLWithString:jsonDictionary[@"_links"][@"self"]];
    } else {
        return nil;
    }
    return self;
}


-(BOOL)validateEmotion:(NSDictionary *)jsonDictionary {
    return (jsonDictionary[@"id"] && jsonDictionary[@"name"] && jsonDictionary[@"_links"][@"self"]);
}

-(NSString *)description {
    return [NSString stringWithFormat:@"JournalEntry id: %@,  name: %@,  link: %@.",_id, _name, _link];
}

@end
