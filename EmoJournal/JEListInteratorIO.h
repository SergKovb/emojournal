//
//  EmotionListInteratorIO.h
//  
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//
//

#import <Foundation/Foundation.h>

@protocol JEListInteractorInput <NSObject>
- (void)loadJournalEntriesList;

@end


@protocol JEListInteractorOutput <NSObject>
- (void)foundJournalEntries:(NSArray *)arrayOfJournalEntries;
@end
