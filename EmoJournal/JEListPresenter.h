//
//  EmotionListPresenter.h
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JEListInteratorIO.h"
#import "JEListModuleInterface.h"
#import "JEListWireframe.h"
#import "JEListViewInterface.h"

@interface JEListPresenter : NSObject <JEListInteractorOutput, JEListModuleInterface>
@property (nonatomic, strong)   id<JEListInteractorInput> interactor; 
@property (nonatomic, weak) JEListWireframe*             listWireframe;

@property (nonatomic, weak) UIViewController<JEListViewInterface> *userInterface;



@end
