//
//  EmotionListInterator.h
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JEListInteratorIO.h"

@interface JEListInteractor : NSObject <JEListInteractorInput>
@property (nonatomic, weak) id <JEListInteractorOutput> output; ////to do protocol for interactor

@end
