//
//  JournalEntry.h
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JournalEntry : NSObject
@property NSString *event;
@property NSString *automaticThoughts;
@property NSString *rationalThoughts;
@property NSString *todo;
@property NSDate *date;
@property NSNumber *id;


-(instancetype)initWithDictionary:(NSDictionary *)jsonDictionary;
-(NSString *)stringDate;

    
@end
