//
//  EmotionListPresenter.m
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import "JEListPresenter.h"

@implementation JEListPresenter


- (void)updateView {
    NSLog(@"EmotionListPresenter -> updateView");
    [self.interactor loadJournalEntriesList];
}


#pragma mark - Interactor output

-(void)foundJournalEntries:(NSArray *)arrayOfJournalEntries {
    if ([arrayOfJournalEntries count] == 0)
    {
        [self.userInterface showNoEntriesMessage];
    }
    else
    {
        [self.userInterface showEntries:arrayOfJournalEntries];
    }
}



@end
