//
//  EmotionListViewController.m
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import "JEListViewController.h"
#import "JournalEntry.h"
#import "JournalEntriesCellView.h"
static NSString* const JournalEntriesListEntryCellIdentifier = @"JournalEntriesListEntry";


@interface JEListViewController ()
@property (nonatomic, strong)   NSArray *arrayOfJournalEntries;
@property (nonatomic, strong)   UITableView *strongTableView;

@end


@implementation JEListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.strongTableView = self.tableView;
    _strongTableView.rowHeight = UITableViewAutomaticDimension;
    _strongTableView.estimatedRowHeight = 80;
    [self configureView];
}


- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"EmotionListViewController -> viewWillAppear");
    [super viewWillAppear:animated];
    [self.eventHandler updateView];
}

- (void)configureView {
    self.navigationItem.title = @"Emotion Journal";
}

- (void)showNoEntriesMessage {
    self.view = self.noContentView;
}




-(void)showEntries:(NSArray *)arrayOfJournalEntries {
    NSLog(@"EmotionListViewController -> showEntries");
    self.view = self.strongTableView;
    _arrayOfJournalEntries = arrayOfJournalEntries;
    [self reloadEntries];
}


- (void)reloadEntries {
    [self.tableView reloadData];
}


#pragma mark - UITableViewDelegate and DataSource Methods



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_arrayOfJournalEntries count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JournalEntry *journalEntry = _arrayOfJournalEntries[indexPath.row];
    JournalEntriesCellView *cell = [self.tableView dequeueReusableCellWithIdentifier:JournalEntriesListEntryCellIdentifier forIndexPath:indexPath];
    
    cell.eventTitle.text = journalEntry.event;
    //cell.dateLabel.text = [journalEntry stringDate];
    //cell.imageView.image = [UIImage imageNamed:section.imageName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}


@end
