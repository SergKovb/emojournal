//
//  JournalEntry.m
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//
#define defaultEvent @"Нет названия"

#import "JournalEntry.h"

@implementation JournalEntry


-(instancetype)initWithDictionary:(NSDictionary *)jsonDictionary {
    self = [super init];
    if (self && [self validateEntry:jsonDictionary]) {
        _id = jsonDictionary[@"id"];
        if (jsonDictionary[@"event"] && ![jsonDictionary[@"event"] isEqualToString:@""]) {
            _event = jsonDictionary[@"event"];
        } else {
            _event = defaultEvent;
        }
        _automaticThoughts = jsonDictionary[@"automaticThoughts"];
        _rationalThoughts = jsonDictionary[@"rationalThoughts"];
        _todo = jsonDictionary[@"todo"];
        NSTimeInterval tstmp = [jsonDictionary[@"timestamp"] doubleValue] / 1000;
        _date = [NSDate dateWithTimeIntervalSince1970:tstmp];
    } else {
        return nil;
    }
    return self;
}

-(BOOL)validateEntry:(NSDictionary *)jsonDictionary {
    return (jsonDictionary[@"id"] && jsonDictionary[@"event"] && jsonDictionary[@"emotion"]);
}

-(NSString *)description {
    return [NSString stringWithFormat:@"JournalEntry id: %@,  event: %@,  date: %@.",_id, _event, _date];
}

-(NSString *)stringDate {
    NSDateFormatter *dateFormatter= [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd.MM | HH:mm"];
    return  [dateFormatter stringFromDate:_date];
}

@end
