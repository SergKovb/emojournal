//
//  EmoJournalDependencies.m
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 31.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import "EmoJournalDependencies.h"

#import "JEListInteractor.h"
#import "JEListPresenter.h"
#import "JEListViewController.h"
#import "JEListWireframe.h"
#import "MainWireframe.h"

@interface EmoJournalDependencies ()
@property (nonatomic, strong) JEListWireframe *listWireframe;
@end

@implementation EmoJournalDependencies

- (id)init {
    if ((self = [super init])) {
        [self configureDependencies];
    }
    return self;
}


- (void)installRootViewControllerIntoWindow:(UIWindow *)window {
    [self.listWireframe presentListInterfaceFromWindow:window];
}


- (void)configureDependencies {
    MainWireframe *mainWireframe = [MainWireframe new];

    JEListWireframe *listWireframe = [JEListWireframe new];
    JEListPresenter *listPresenter = [JEListPresenter new];
    JEListInteractor *listInteractor = [JEListInteractor new];
    
    listInteractor.output = listPresenter;
    
    listPresenter.interactor = listInteractor;
    listPresenter.listWireframe = listWireframe;
    
    listWireframe.listPresenter = listPresenter;
    listWireframe.mainWireframe = mainWireframe;
    self.listWireframe = listWireframe;

}


@end
