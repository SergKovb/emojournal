//
//  EmoJournalDependencies.h
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 31.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmoJournalDependencies : NSObject
- (void)installRootViewControllerIntoWindow:(UIWindow *)window;

@end
