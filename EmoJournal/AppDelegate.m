//
//  AppDelegate.m
//  EmoJournal
//
//  Created by Sergey Kovbasyuk on 30.05.17.
//  Copyright © 2017 Sergey Kovbasyuk. All rights reserved.
//

#import "AppDelegate.h"

#import "EmoJournalDependencies.h"

@interface AppDelegate ()
@property (nonatomic, strong) EmoJournalDependencies *dependencies;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
    EmoJournalDependencies *dependencies = [[EmoJournalDependencies alloc] init];
    self.dependencies = dependencies;
    
    [self.dependencies installRootViewControllerIntoWindow:self.window];
    return YES;
}



@end
